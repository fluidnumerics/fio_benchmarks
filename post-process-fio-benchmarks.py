#!/usr/bin/env python

import os
import subprocess
import shlex
import json
import yaml
import json
import datetime
import numpy as np

def get_metadata_from_filepath( filepath, fs_name ):

  experiment = {}
  splitname = filepath.split("/")
  # First entry in splitname is "results"

  # Second entry in splitname is <FILESYSTEM>
  experiment['fileserver'] = fs_name

  # Third entry is the number of clients and the number of IO tasks
  # embedded in the subdirectory name as "clients-$NCLIENTS-tasks-$NTASKS"
  client_tasks = splitname[2].split("-")
  experiment['ntasks']   = client_tasks[3]
  experiment['nclients'] = client_tasks[1]
  experiment['label']    = fs_name+':'+client_tasks[3]+' tasks, '+client_tasks[1]+' clients' 

  # Fourth entry is the actual json filename, which has
  # the following format 
  # ${ACTION}_${MOUNT_POINT}-${FILESIZE}_${SLURM_PROCID}_${BLOCKSIZE}
  expvars = splitname[3].split("-")
  experiment['action']    = expvars[0].split("_")[0]
  experiment['filesize']  = int(expvars[1].split("_")[0][:-1])
  experiment['client_id'] = expvars[1].split("_")[1]


  # Now we extract the experiment data
  with open( filepath, 'r' ) as stream:
    try:
      data = json.load( stream )
    except :
      print( 'Failed to load '+filepath )
      return experiment

  experiment['fio_version'] = data['fio version']
  experiment['time']        = data['time']
  experiment['elapsed']     = data['jobs'][0]['elapsed']
  experiment['options']     = data['jobs'][0]['job options']

  if 'write' in experiment['action'] :
    experiment['stats'] = {}
    experiment['stats']['io_bytes']  = data['jobs'][0]['write']['io_bytes']
    experiment['stats']['bw']        = data['jobs'][0]['write']['bw']
    experiment['stats']['iops']      = data['jobs'][0]['write']['iops']
    experiment['stats']['runtime']   = data['jobs'][0]['write']['runtime']
    experiment['stats']['bw_min']    = data['jobs'][0]['write']['bw_min']
    experiment['stats']['bw_max']    = data['jobs'][0]['write']['bw_max']
    experiment['stats']['bw_mean']   = data['jobs'][0]['write']['bw_mean']
    experiment['stats']['bw_dev']    = data['jobs'][0]['write']['bw_dev']
    experiment['stats']['iops_min']  = data['jobs'][0]['write']['iops_min']
    experiment['stats']['iops_max']  = data['jobs'][0]['write']['iops_max']
    experiment['stats']['iops_mean'] = data['jobs'][0]['write']['iops_mean']
    experiment['stats']['iops_dev']  = data['jobs'][0]['write']['iops_stddev']

  elif 'read' in experiment['action'] :
    experiment['stats'] = {}
    experiment['stats']['io_bytes']  = data['jobs'][0]['read']['io_bytes']
    experiment['stats']['bw']        = data['jobs'][0]['read']['bw']
    experiment['stats']['iops']      = data['jobs'][0]['read']['iops']
    experiment['stats']['runtime']   = data['jobs'][0]['read']['runtime']
    experiment['stats']['bw_min']    = data['jobs'][0]['read']['bw_min']
    experiment['stats']['bw_max']    = data['jobs'][0]['read']['bw_max']
    experiment['stats']['bw_mean']   = data['jobs'][0]['read']['bw_mean']
    experiment['stats']['bw_dev']    = data['jobs'][0]['read']['bw_dev']
    experiment['stats']['iops_min']  = data['jobs'][0]['read']['iops_min']
    experiment['stats']['iops_max']  = data['jobs'][0]['read']['iops_max']
    experiment['stats']['iops_mean'] = data['jobs'][0]['read']['iops_mean']
    experiment['stats']['iops_dev']  = data['jobs'][0]['read']['iops_stddev']
  
   
  return experiment

#END get_metadata_from_filepath( filepath )

def load_config( ):

  with open("./filesystems.yaml", 'r') as stream:
    try:
      config = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
      print(exc)

  return config

#END load_config()

def clean_data_for_plots( data ):

  experiment_labels = []

  for experiment in data['experiment']:
    if experiment['label'] not in experiment_labels:
      experiment_labels.append(experiment['label'])
    
  for label in experiment_labels:
    print( label )

  results_to_plot = {}
  results_to_plot['experiment'] = []
  
  # Build up an object with the unique labels first
  for label in experiment_labels:
    obj = {}
    obj['label'] = label
    obj['read_bandwidth'] = []
    obj['read_blocksize'] = []
    obj['write_bandwidth'] = []
    obj['write_blocksize'] = []
    obj['nclients']  = 0
    obj['ntasks']    = 0
    obj['xlabel']    = 'Block Size (KB)'
    obj['ylabel']    = 'Bandwidth (MB/s)'
    results_to_plot['experiment'].append( obj ) 

  for experiment in data['experiment']:
    exp_label = experiment['label']
    
    # Find which experiment the data should be reported to
    k=0
    for result in results_to_plot['experiment']:
      if exp_label == result['label']:
        break
      else:
        k+=1
  
    results_to_plot['experiment'][k]['nclients'] = experiment['nclients']
    results_to_plot['experiment'][k]['ntasks']   = experiment['ntasks']
    # Now store the bandwidth and block size
    if 'stats' in experiment.keys():
      
      if 'read' in experiment['action']:
        results_to_plot['experiment'][k]['read_bandwidth'].append( experiment['stats']['bw_mean']/1024 )
        results_to_plot['experiment'][k]['read_blocksize'].append( float( experiment['options']['bs'][:-1] ) )
      
      elif 'write' in experiment['action']:
        results_to_plot['experiment'][k]['write_bandwidth'].append( experiment['stats']['bw_mean']/1024 )
        results_to_plot['experiment'][k]['write_blocksize'].append( float( experiment['options']['bs'][:-1] ) )
    else:
      print('Failed to find stats for '+exp_label+'(client id '+experiment['client_id']+')')

  # Now we need to sort the bandwidth and blocksize in order of increasing blocksize
  for k in range( len(results_to_plot['experiment']) ):
    bw_indices = np.asarray(results_to_plot['experiment'][k]['read_blocksize']).argsort()
    bs = []
    bw = []
    for j in range( len( bw_indices ) ):
      bs.append( results_to_plot['experiment'][k]['read_blocksize'][bw_indices[j]] )
      bw.append( results_to_plot['experiment'][k]['read_bandwidth'][bw_indices[j]] )
                  
    results_to_plot['experiment'][k]['read_blocksize'] = bs
    results_to_plot['experiment'][k]['read_bandwidth'] = bw
    
    bw_indices = np.asarray(results_to_plot['experiment'][k]['write_blocksize']).argsort()
    bs = []
    bw = []
    for j in range( len( bw_indices ) ):
      bs.append( results_to_plot['experiment'][k]['write_blocksize'][bw_indices[j]] )
      bw.append( results_to_plot['experiment'][k]['write_bandwidth'][bw_indices[j]] )
                  
    results_to_plot['experiment'][k]['write_blocksize'] = bs
    results_to_plot['experiment'][k]['write_bandwidth'] = bw 
  
# For those experiments with more than one client, we need to accumulate the bandwidth
  k=0
  for experiment in results_to_plot['experiment']:
    if int(experiment['ntasks']) > 1:
      bs = experiment['read_blocksize'][0::int(experiment['ntasks'])]    
      bw = []
      for i in range(len(experiment['read_bandwidth'])/int(experiment['ntasks'])):
        lo = i*int(experiment['ntasks'])
        hi = (i+1)*int(experiment['ntasks'])
        sum_bw = np.sum( np.asarray( experiment['read_bandwidth'][lo:hi] ) )
        bw.append( sum_bw )
        
      results_to_plot['experiment'][k]['read_bandwidth'] = bw
      results_to_plot['experiment'][k]['read_blocksize'] = bs
      
      bs = experiment['write_blocksize'][0::int(experiment['ntasks'])]    
      bw = []
      for i in range(len(experiment['write_bandwidth'])/int(experiment['ntasks'])):
        lo = i*int(experiment['ntasks'])
        hi = (i+1)*int(experiment['ntasks'])
        sum_bw = np.sum( np.asarray( experiment['write_bandwidth'][lo:hi] ) )
        bw.append( sum_bw )
        
      results_to_plot['experiment'][k]['write_bandwidth'] = bw
      results_to_plot['experiment'][k]['write_blocksize'] = bs
    k+=1

  return results_to_plot
  
#END clean_data_for_plots


master_dict = load_config()

master_dict['experiment'] = []

# Aggregate results
for fs in master_dict['filesystem']:
   for exp_dir in os.listdir('./results/'+fs['name']+'/'):
     for exp in os.listdir('./results/'+fs['name']+'/'+exp_dir+'/'):

       exp_file = 'results/'+fs['name']+'/'+exp_dir+'/'+exp
       exp = get_metadata_from_filepath( exp_file, fs['name'] )
       master_dict['experiment'].append(exp)


# Write results to file
today = datetime.datetime.today().strftime("%m_%d_%Y_%H_%M_%S")
with open('results/benchmarks_'+today+'.json', 'w') as outfile:  
    json.dump(master_dict, outfile)

plot_results = clean_data_for_plots( master_dict )

with open('results/cleaned_data_'+today+'.json', 'w') as outfile:
    json.dump(plot_results, outfile)

subprocess.call(shlex.split('gsutil cp results/cleaned_data_'+today+'.json gs://filesystem-benchmarks/'))
subprocess.call(shlex.split('gsutil cp results/benchmarks_'+today+'.json gs://filesystem-benchmarks/'))
