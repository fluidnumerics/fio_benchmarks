#!/bin/bash

#export NCLIENTS=1
export NTASKS=10
export NTASKS_PER_NODE=10
export FILESIZE="10G"
export MOUNTPOINT="/mnt/lustre/joeschoonover/stripesize_32M/"
export OUTDIR="./results/lustre_3TB_SSD_32M/clients-1-tasks-10/"


mkdir -p ${OUTDIR}

#IOACTIONS=("write" \
#           "read" \
#           "randwrite" \
#           "randread" )

IOACTIONS=("randwrite" \
           "randread" )

#BLOCKSIZES=("4K" "16K" "32K" "64K" "128K" "256K" "512K" "1024K" "2048K" "4096K" "8192K" "16384K" "32768K")
#BLOCKSIZES=("256K" "512K" "1024K" "2048K" "4096K" "8192K" "16384K" "32768K")
BLOCKSIZES=("128K" "256K" "512K" "1024K" "2048K" "4096K" "8192K" "16384K" "32768K")


n_actions=${#IOACTIONS[@]}
n_blocks=${#BLOCKSIZES[@]}

for (( j=0; j<${n_actions}; j++ ));
do
  for (( i=0; i<${n_blocks}; i++ ));
  do
    export IOACTION=${IOACTIONS[$j]} 
    export BLOCKSIZE=${BLOCKSIZES[$i]}

    echo "$IOACTION $BLOCKSIZE"

    sbatch --export=all \
           --ntasks=${NTASKS} \
           --ntasks-per-node=${NTASKS_PER_NODE} \
           --job-name=fio_bench \
           --time=4:00:00 \
           --dependency=singleton \
           ./fio_benchmark.slurm

  done
done

