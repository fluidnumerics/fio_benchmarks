
import subprocess
import shlex


APPS_DIR='/apps'

proc = subprocess.Popen(shlex.split('squeue --noheader'), stdout=subprocess.PIPE)

jobids = []
for jobs in proc.stdout.readlines() :
  jobids.append( jobs.split()[0] )

print( jobids )

# If there are jobs, then we need to cancel them
cmd = APPS_DIR+'/slurm/current/bin/scancel '
if( len( jobids ) > 0  ):
  for job in jobids :
      cmd+=job+' '

print cmd
