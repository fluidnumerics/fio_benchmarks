#!/bin/bash 

# Cleans out error messages from fio in json data sets
find . -type f -name "*.json" -exec sed -i '/clock setaffinity failed/d' {} +
find . -type f -name "*.json" -exec sed -i '/fio: native_fallocate call failed: Operation not supported/d' {} +

