#!/bin/bash
#
# Example : 
#
#     srun -n ${SLURM_NTASKS} bash ./fio_benchmark_singleton.sh randwrite 10G /mnt/elastifile 1024K
#

ACTION=$1
FILESIZE=$2
LOCATION=$3
BS=$4
ODIR=$5

# For the output csv, replace the "/" with "_"
FILOC=$(echo $LOCATION | sed 's/\//_/g')
FILENAME="${ACTION}${FILOC}-${FILESIZE}_${SLURM_PROCID}"


FLOC=${LOCATION} FSIZE=${FILESIZE} RWACT=${ACTION} BLOCKSIZE=${BS} FNAME="test_${SLURM_PROCID}.txt" fio slurm-cluster.fio --output="${ODIR}/${FILENAME}_${BS}.json" --output-format=json





